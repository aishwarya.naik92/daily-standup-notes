# 9/13 Group 1 - Standup

-   Sandra

    -   Currently, have the wedding planner completed. All testing for wedding-planner worked, attempting postman requests and test they are working and begin implmeneting into the frontend.

-   Ash

    -   Currently, completed the backend wedding planner. Front end address 2/3 pages. Authorization page requires debugging, connection issues with front end. Moving to messing and file upload. Attempting to complete theses backends and debug.

-   Michael

    -   Currently, on back-end wedding planner. Able to create weddings, weddingbyID, get all weddings. Issues with update wedding ID by idea. Testing with postgresQL. Issues with update and deletion for backend.

-   Donavan

    -   Currently, completed wedding-planner, authorization. Need frontend for authorization. After, need messing and authorizaiton services. No issues YET

-   Charles
    -   Currently, have the wedding-planner completed, excluding expenses on front end. Authorizaiton is fully implemented and also the file upload. Chat services and CSS.

# 9/14 Group 1 - Stand Up

-   Sandra

    -   Began react, working on building out the planner page for wedding and expense. Worked on incorporating some CSS. Errors on postman after working, potentially server isn't up and running.

-   Ash

    -   File upload completed on fron and back end. Cocentrating on front-end posting, updating and deleting. Issues with react attempting to assemble the components, reverting from grid approach.

-   Donovan

    -   Upload front and back-end completed. Working on login authorization and having issues passing credentials with credentials between components. Potentially Link to help solve this.

-   Michael

    -   Wedding-planner servives are now able to complete updates by wedding ID working. Deletion comes up with does not exist consistently. Began working on React and attempting some simple requests.

-   Charles
    -   Everything working in react, except for messing service. One minor issue with re-assigning the reference weddingID in expense-entry after using a redux store value from the wedding-entry component to also assign the value.
